import cvxpy as cvx
import pandas as pd


class Problem:
    def __init__(self, N, verbose, react=True, loss=True, conic=True, grid=True, DSO=True):
        self.react = react
        self.loss_mod = loss
        self.conic = conic
        self.grid = grid
        self.DSO = DSO

        # Market layer
        self.create_agents(N)
        self.create_communication(N)
        self.create_ag_variables()
        self.create_ag_constraints()
        self.create_ag_objfun(N)

        # Power flow
        self.create_nodes(N)
        self.create_lines(N)
        if self.loss_mod:
            self.estimate_loss_factors(N)
            self.create_loss_allocation(N)
        self.create_PF_variables(N)
        self.create_PF_constraints(N)

        self.solve(verbose)

    def create_agents(self, N):
        self.agents = pd.DataFrame(columns=['bus', 'Pmin', 'Pmax', 'lin', 'quad', 'ID'])
        for t in N.TSOs:
            TSO = N.TSOs[t].agent[['bus', 'Pmin', 'Pmax', 'lin', 'quad']]
            TSO['ID'] = 'T' + str(t)
            self.agents = self.agents.append(TSO)

        for d in N.DSOs:
            DSO = N.DSOs[d].agent[['bus', 'Pmin', 'Pmax', 'lin', 'quad']]
            CM = pd.DataFrame([[pd.np.nan, 0, 0, 0, 0]], index=['CM' + str(d)],
                              columns=['bus', 'Pmin', 'Pmax', 'lin', 'quad'])
            DSO = DSO.append(CM)
            DSO['ID'] = 'D' + str(d)
            self.agents = self.agents.append(DSO)

        self.agents['K'] = abs(self.agents[['Pmin','Pmax']]).max(axis=1)
        self.ag_idx_tso = self.agents[self.agents.ID.str.contains('T')].index
        self.ag_idx_dso = self.agents[self.agents.ID.str.contains('D')].index

    def create_communication(self, N):
        self.communication = pd.DataFrame(data=0., index=self.agents.index, columns=self.agents.index)
        for d in N.DSOs:
            idx_ag = self.agents[(self.agents.ID == 'D' + str(d)) & (~self.agents.bus.isna())].index
            idx_CM = self.agents[(self.agents.ID == 'D' + str(d)) & (self.agents.bus.isna())].index
            self.communication.at[idx_ag, idx_CM] = 1
            self.communication.at[idx_CM, idx_ag] = 1

        idx_gen = self.agents[(self.agents.ID.str.contains('T')) & (self.agents.Pmax > 0)].index
        idx_con = self.agents[(self.agents.ID.str.contains('T')) & (self.agents.Pmin < 0)].index
        self.communication.at[idx_gen, idx_con] = 1
        self.communication.at[idx_con, idx_gen] = 1

        idx_CM = self.agents[self.agents.bus.isna()].index
        idx_p2p = self.agents[self.agents.ID.str.contains('T')].index
        self.communication.at[idx_p2p, idx_CM] = 1
        self.communication.at[idx_CM, idx_p2p] = 1

    def create_ag_variables(self):
        self.Pag = cvx.Variable(name='Pag', shape=len(self.agents))
        self.partner_i, self.partner_j = pd.np.where(pd.np.triu(pd.np.array(self.communication == 1)))
        self.Tag_i = cvx.Variable(name='Tag_i', shape=len(self.partner_i))
        self.Tag_j = cvx.Variable(name='Tag_j', shape=len(self.partner_j))
        if self.loss_mod:
            self.Wag_i = cvx.Variable(name='Wag_i', shape=len(self.partner_i))
            self.Wag_j = cvx.Variable(name='Wag_j', shape=len(self.partner_j))
            # self.Zag = cvx.Variable(name='Zag', shape=len(self.agents))

    def create_ag_constraints(self):
        self.ag_UB = self.Pag <= self.agents.Pmax.values
        self.ag_LB = self.Pag >= self.agents.Pmin.values

        self.ag_pow_bal = []
        for i in range(len(self.agents)):
            if (i in self.partner_i) and (i in self.partner_j):
                if self.loss_mod and self.grid:
                    self.ag_pow_bal += [
                        cvx.sum(self.Tag_i[self.partner_i == i]) + cvx.sum(self.Tag_j[self.partner_j == i]) + cvx.sum(
                            self.Wag_i[self.partner_i == i]) + cvx.sum(self.Wag_j[self.partner_j == i]) == self.Pag[i]]
                else:
                    self.ag_pow_bal += [
                        cvx.sum(self.Tag_i[self.partner_i == i]) + cvx.sum(self.Tag_j[self.partner_j == i]) == self.Pag[
                            i]]
            elif (i in self.partner_i):
                if self.loss_mod and self.grid:
                    self.ag_pow_bal += [cvx.sum(self.Tag_i[self.partner_i == i]) + cvx.sum(
                            self.Wag_i[self.partner_i == i]) == self.Pag[i]]
                else:
                    self.ag_pow_bal += [cvx.sum(self.Tag_i[self.partner_i == i]) == self.Pag[i]]
            elif (i in self.partner_j):
                if self.loss_mod and self.grid:
                    self.ag_pow_bal += [cvx.sum(self.Tag_j[self.partner_j == i]) + cvx.sum(
                            self.Wag_j[self.partner_j == i]) == self.Pag[i]]
                else:
                    self.ag_pow_bal += [cvx.sum(self.Tag_j[self.partner_j == i]) == self.Pag[i]]
        
        # if self.loss_mod and self.grid:
        #     self.trades_rec = self.Tag_i + self.Wag_i == -self.Tag_j + self.Wag_j
        # else:
        self.trades_rec = self.Tag_i == -self.Tag_j
        self.ag_constraints = [
                                  self.ag_UB, self.ag_LB, self.trades_rec
                              ] + self.ag_pow_bal

    def create_ag_objfun(self, N):
        self.obj_ag = self.Pag * self.agents.lin.values #+ 0.01*cvx.norm(self.Tag_i, 1) + 0.01*cvx.norm(self.Tag_j, 1) 


    def create_nodes(self, N):
        self.nodes = pd.DataFrame(columns=['type', 'ID', 'Vmax', 'Vmin'])
        for t in N.TSOs:
            TSO_n = N.TSOs[t].bus[['type', 'Vmax', 'Vmin']]
            TSO_n.insert(1, 'ID', 'T' + str(t))
            self.nodes = self.nodes.append(TSO_n)

        for d in N.DSOs:
            DSO_n = N.DSOs[d].bus[['type', 'Vmax', 'Vmin']]
            DSO_n.insert(1, 'ID', 'D' + str(d))
            self.nodes = self.nodes.append(DSO_n)

        self.nodes.reset_index(drop=True, inplace=True)
        self.n_idx_tso = self.nodes[self.nodes.ID.str.contains('T')].index
        self.n_idx_dso = self.nodes[self.nodes.ID.str.contains('D')].index

    def create_lines(self, N):
        self.lines = pd.DataFrame(columns=['fbus', 'tbus', 'r', 'x', 'b', 'cap', 'ID'])
        self.lines_HVDC = pd.DataFrame(columns=['fbus', 'tbus', 'r', 'cap', 'x1', 'x2', 'x3', 'x4', 'ID'])       

        for t in N.TSOs:
            TSO_l = N.TSOs[t].branch[['fbus', 'tbus', 'r', 'x', 'b', 'rateA']]
            TSO_l.insert(6, 'ID', 'T' + str(t))
            TSO_l.columns = self.lines.columns
            self.lines = self.lines.append(TSO_l)

            TSO_HVDC = N.TSOs[t].branchHVDC[['fbus', 'tbus', 'r', 'rateA', 'x1', 'x2', 'x3', 'x4']]
            TSO_HVDC.insert(8, 'ID', 'T' + str(t))
            TSO_HVDC.columns = self.lines_HVDC.columns
            self.lines_HVDC = self.lines_HVDC.append(TSO_HVDC)

        for d in N.DSOs:
            DSO_l = N.DSOs[d].branch[['fbus', 'tbus', 'r', 'x', 'b', 'rateA']]
            DSO_l.insert(6, 'ID', 'D' + str(d))
            DSO_l.columns = self.lines.columns
            self.lines = self.lines.append(DSO_l)

        self.lines.reset_index(drop=True, inplace=True)
        self.lines_HVDC.reset_index(drop=True, inplace=True)
        self.l_idx_tso = self.lines[self.lines.ID.str.contains('T')].index
        self.l_idx_dso = self.lines[self.lines.ID.str.contains('D')].index

    def estimate_loss_factors(self, N, width_TSO=50, width_DSO=1):
        linseg = pd.np.arange(0, max(max(self.lines_HVDC.cap), max(self.lines.cap)) * N.Sb + width_TSO, width_TSO)
        nsegHVDC = pd.np.ceil(self.lines_HVDC.cap * N.Sb / width_TSO).astype(int)
        nsegAC = pd.np.ceil(self.lines.loc[self.l_idx_tso].cap * N.Sb / width_TSO).astype(int)
        
        if self.DSO:
            linseg_DSO = pd.np.arange(0, max(self.lines.loc[self.l_idx_dso].cap) * N.Sb + width_DSO, width_DSO)
            nsegAC_DSO = pd.np.ceil(self.lines.loc[self.l_idx_dso].cap * N.Sb / width_DSO).astype(int)
            self.LF_AC = pd.DataFrame(0., index=pd.MultiIndex.from_product([self.lines.index, ['c', 'm']],
                                                                       names=['lines', 'coef']),
                                  columns=range(max(nsegAC.max(), nsegAC_DSO.max())))
        else:
            self.LF_AC = pd.DataFrame(0., index=pd.MultiIndex.from_product([self.lines.index, ['c', 'm']],
                                                                       names=['lines', 'coef']),
                                  columns=range(nsegAC.max()))

            

        # HVDC loss factors
        a_inv = self.lines_HVDC.x1
        a_rec = self.lines_HVDC.x2
        R_DC = self.lines_HVDC.r
        b_DC = self.lines_HVDC.x3
        c_DC = self.lines_HVDC.x4
        self.LF_HVDC = pd.DataFrame(0., index=pd.MultiIndex.from_product([self.lines_HVDC.index, ['c', 'm']],
                                                                         names=['lines', 'coef']),
                                    columns=range(nsegHVDC.max()))
        for l in self.lines_HVDC.index:
            for j in range(nsegHVDC.loc[l]):
                X = pd.np.linspace(linseg[j], linseg[j + 1], 100) / N.Sb
                Y = (a_inv.loc[l] + a_rec.loc[l] + R_DC.loc[l]) * X * X + 2 * b_DC.loc[l] * X + 2 * c_DC.loc[l]
                m, c = pd.np.polyfit(X, Y, 1)
                if j == 0 and c < 0:
                    c = 0
                self.LF_HVDC.loc[l].at['m', j] = m
                self.LF_HVDC.loc[l].at['c', j] = c


        # AC loss factors
        R_AC = self.lines.r
        for l in self.l_idx_tso:
            if nsegAC.loc[l] == 0:
                j = 0
                X = pd.np.linspace(linseg[j], linseg[j + 1], 100) / N.Sb
                Y = R_AC.loc[l] * X * X
                m, c = pd.np.polyfit(X, Y, 1)
                if j == 0 and c < 0:
                    c = 0
                self.LF_AC.loc[l].at['m', j] = m
                self.LF_AC.loc[l].at['c', j] = c
            else:
                for j in range(nsegAC.loc[l]):
                    X = pd.np.linspace(linseg[j], linseg[j + 1], 100) / N.Sb
                    Y = R_AC.loc[l] * X * X
                    m, c = pd.np.polyfit(X, Y, 1)
                    if j == 0 and c < 0:
                        c = 0
                    self.LF_AC.loc[l].at['m', j] = m
                    self.LF_AC.loc[l].at['c', j] = c

        for l in self.l_idx_dso:
            if nsegAC_DSO.loc[l] == 0:
                j = 0
                X = pd.np.linspace(linseg_DSO[j], linseg_DSO[j + 1], 100) / N.Sb
                Y = R_AC.loc[l] * X * X
                m, c = pd.np.polyfit(X, Y, 1)
                if j == 0 and c < 0:
                    c = 0
                self.LF_AC.loc[l].at['m', j] = m
                self.LF_AC.loc[l].at['c', j] = c
            else:
                for j in range(nsegAC_DSO.loc[l]):
                    X = pd.np.linspace(linseg_DSO[j], linseg_DSO[j + 1], 100) / N.Sb
                    Y = R_AC.loc[l] * X * X
                    m, c = pd.np.polyfit(X, Y, 1)
                    if j == 0 and c < 0:
                        c = 0
                    self.LF_AC.loc[l].at['m', j] = m
                    self.LF_AC.loc[l].at['c', j] = c



    def create_loss_allocation(self, N):
        self.physical_allocation(N)

        self.economic_allocation(N)


    def physical_allocation(self, N):
        # TSO allocation policy
        lines = self.lines.loc[self.l_idx_tso]
        self.flows_to_nodes_tso = pd.DataFrame(0., index=self.l_idx_tso, columns=self.nodes.index)
        self.flows_to_nodes_tso_HVDC = pd.DataFrame(0., index=self.lines_HVDC.index, columns=self.nodes.index)
        for l in lines.index:
            fbus = lines.loc[l].fbus
            tbus = lines.loc[l].tbus
            self.flows_to_nodes_tso.at[l, fbus] = 0.5
            self.flows_to_nodes_tso.at[l, tbus] = 0.5

        for l in self.lines_HVDC.index:
            fbus = self.lines_HVDC.loc[l].fbus
            tbus = self.lines_HVDC.loc[l].tbus
            self.flows_to_nodes_tso_HVDC.at[l, fbus] = 0.5
            self.flows_to_nodes_tso_HVDC.at[l, tbus] = 0.5

        # DSO allocation policy
        self.flows_to_nodes_dso = pd.DataFrame(0., index=self.l_idx_dso, columns=self.nodes.index)
        for d in N.DSOs:
            lines = self.lines[self.lines.ID == 'D' + str(d)]
            bus = self.agents[self.agents.ID == 'D' + str(d)].dropna().bus
            for l in lines.index:
                fbus = self.lines.loc[l].fbus
                tbus = self.lines.loc[l].tbus
                self.flows_to_nodes_dso.at[l, fbus] = 0.5
                self.flows_to_nodes_dso.at[l, tbus] = 0.5


    def economic_allocation(self, N):
        # Agents allocation policy
        self.nodes_to_agents = pd.DataFrame(0., index=self.agents.index, columns=self.nodes.index)

        for t in N.TSOs:
            n_idx_tso = self.nodes[self.nodes.ID=='T'+str(t)].index
            ag_idx_tso = self.agents[self.agents.ID=='T'+str(t)].index
            alpha = N.TSOs[t].alpha
            social = N.TSOs[t].social
            K_h = self.agents.loc[ag_idx_tso].K.sum()
            N_h = len(ag_idx_tso)
            for b in n_idx_tso:
                agents = self.agents[self.agents.bus == b].index
                K_j = self.agents.loc[agents].K.sum()
                N_j = len(agents)
                if N_j > 0:
                    for a in agents:
                        self.nodes_to_agents.at[a, b] = ((1-social)*(alpha*self.agents.loc[a].K/K_j + (1-alpha)/N_j)
                                                           + social*(alpha*self.agents.loc[a].K/K_h + (1-alpha)/N_h) )
                    for a in ag_idx_tso:
                        if a not in agents:
                            self.nodes_to_agents.at[a, b] = social*(alpha*self.agents.loc[a].K/K_h + (1-alpha)/N_h)
                else:
                    for a in ag_idx_tso:
                        self.nodes_to_agents.at[a, b] = alpha*self.agents.loc[a].K/K_h + (1-alpha)/N_h
                    

        for d in N.DSOs:
            n_idx_dso = self.nodes[self.nodes.ID=='D'+str(d)].index
            ag_idx_dso = self.agents[(self.agents.ID=='D'+str(d)) & ~(self.agents.bus.isna())].index
            alpha = N.DSOs[d].alpha
            social = N.DSOs[d].social
            K_h = self.agents.loc[ag_idx_dso].K.sum()
            N_h = len(ag_idx_dso)
            for b in n_idx_dso:
                agents = self.agents[self.agents.bus == b].index
                K_j = self.agents.loc[agents].K.sum()
                N_j = len(agents)
                if N_j > 0:
                    for a in agents:
                        self.nodes_to_agents.at[a, b] = ((1-social)*(alpha*self.agents.loc[a].K/K_j + (1-alpha)/N_j)
                                                           + social*(alpha*self.agents.loc[a].K/K_h + (1-alpha)/N_h))
                    for a in ag_idx_dso:
                        if a not in agents:
                            self.nodes_to_agents.at[a, b] = social*(alpha*self.agents.loc[a].K/K_h + (1-alpha)/N_h)
                else:
                    for a in ag_idx_dso:
                        self.nodes_to_agents.at[a, b] = alpha*self.agents.loc[a].K/K_h + (1-alpha)/N_h
        

    def create_PF_variables(self, N):
        self.flow = cvx.Variable(name='flow', shape=len(self.lines))
        self.flow_HVDC = cvx.Variable(name='flow_HVDC', shape=len(self.lines_HVDC))
        if self.DSO:
            self.flow_TD = cvx.Variable(name='flow_TSO_DSO', shape=len(N.TSO_DSO))
            self.theta = cvx.Variable(name='theta', shape=len(self.n_idx_dso))

        self.injections = cvx.Variable(name='inj', shape=len(self.n_idx_tso))

        if self.react and self.DSO:
            self.V = cvx.Variable(name='theta', shape=len(self.n_idx_dso))
            self.Q = cvx.Variable(name='Q', shape=len(self.ag_idx_dso))
            self.q_flow = cvx.Variable(name='q_flow', shape=len(self.l_idx_dso))

        if self.loss_mod:
            self.line_loss = cvx.Variable(name='line_loss', shape=len(self.lines))
            self.line_loss_HVDC = cvx.Variable(name='line_loss_HVDC', shape=len(self.lines_HVDC))
            # self.node_loss = cvx.Variable(name='node_loss', shape=len(self.nodes))
        
        # self.z_i = cvx.Variable(name='trade_loss_i', shape=len(self.partner_i))
        # self.z_j = cvx.Variable(name='trade_loss_j', shape=len(self.partner_j))

    def create_PF_constraints(self, N):
        self.create_TSO_constraints(N)
        if self.DSO:
            self.create_DSO_constraints(N)
        else:
            self.DSO_constraints = []
        self.create_losses_constraints(N)


    def create_TSO_constraints(self, N):
        # TSO constraints
        self.B = self.create_B(N)
        self.I_ag, self.I_fl, self.I_fl_HVDC, self.I_td = self.create_incidence(N)
        self.PTDF = self.create_PTDF(N)
        self.flow_TSO_def = self.flow[self.l_idx_tso] == self.PTDF.values * self.injections

        if self.DSO:
            if self.loss_mod and self.grid:
                self.pow_bal_tso = -self.I_ag.loc[self.n_idx_tso].values * self.Pag + self.injections + cvx.reshape(
                    self.I_fl_HVDC.values * self.flow_HVDC, (len(self.n_idx_tso),)) + cvx.reshape(
                    self.I_td.loc[self.n_idx_tso].values * self.flow_TD, (len(self.n_idx_tso),)) + self.flows_to_nodes_tso.values.T[self.n_idx_tso,:]*self.line_loss[
                                    self.l_idx_tso] == 0  # what about hvdc losses???
            else:
                self.pow_bal_tso = -self.I_ag.loc[self.n_idx_tso].values * self.Pag + self.injections + cvx.reshape(
                    self.I_fl_HVDC.values * self.flow_HVDC, (len(self.n_idx_tso),)) + cvx.reshape(
                    self.I_td.loc[self.n_idx_tso].values * self.flow_TD, (len(self.n_idx_tso),)) == 0
        else:
            if self.loss_mod and self.grid:
                self.pow_bal_tso = -self.I_ag.loc[self.n_idx_tso].values * self.Pag + self.injections + cvx.reshape(
                    self.I_fl_HVDC.values * self.flow_HVDC, (len(self.n_idx_tso),)) + self.flows_to_nodes_tso.values.T[self.n_idx_tso,:]*self.line_loss[
                                    self.l_idx_tso] == 0
            else:
                self.pow_bal_tso = -self.I_ag.loc[self.n_idx_tso].values * self.Pag + self.injections + cvx.reshape(
                    self.I_fl_HVDC.values * self.flow_HVDC, (len(self.n_idx_tso),)) == 0

        if self.grid:
            self.flow_UB = self.flow[self.l_idx_tso] <= self.lines.loc[self.l_idx_tso].cap.values
            self.flow_LB = self.flow[self.l_idx_tso] >= -self.lines.loc[self.l_idx_tso].cap.values
            self.flow_HVDC_UB = self.flow_HVDC <= self.lines_HVDC.cap.values
            self.flow_HVDC_LB = self.flow_HVDC >= -self.lines_HVDC.cap.values
            self.TSO_constraints = [
                self.flow_UB, self.flow_LB, self.flow_HVDC_UB, self.flow_HVDC_LB, self.pow_bal_tso, self.flow_TSO_def,
            ]
        else:
            self.TSO_constraints = [self.pow_bal_tso, self.flow_TSO_def,]


    def create_DSO_constraints(self, N):
        # DSO constraints
        self.theta_ref_DSO = []
        self.nodes_dso = self.nodes.loc[self.n_idx_dso].reset_index()
        for k in N.DSOs.keys():
            self.theta_ref_DSO += [
                self.theta[self.nodes_dso[(self.nodes_dso.type == 3) & (self.nodes_dso.ID == 'D' + str(k))].index] == 0
            ]

        self.B_dso, self.G_dso, self.B_star, self.b_zero = self.create_B_dso(N)
        self.I_fl_dso = self.create_incidence_dso(N)
        if self.loss_mod and self.grid:
            self.pow_bal_dso = -self.I_ag.loc[self.n_idx_dso].values * self.Pag + self.I_fl_dso.values * self.flow[
                self.l_idx_dso] + cvx.reshape(self.I_td.loc[self.n_idx_dso].values * self.flow_TD, (len(self.n_idx_dso),)) + self.flows_to_nodes_dso.values.T[self.n_idx_dso,:]*self.line_loss[
                                    self.l_idx_dso] == 0
        else:
            self.pow_bal_dso = -self.I_ag.loc[self.n_idx_dso].values * self.Pag + self.I_fl_dso.values * self.flow[
                self.l_idx_dso] + cvx.reshape(self.I_td.loc[self.n_idx_dso].values * self.flow_TD, (len(self.n_idx_dso),)) == 0

        if self.react:
            Q_max = self.agents.loc[self.ag_idx_dso][['Pmax', 'Pmin']]
            Q_max.Pmin = -Q_max.Pmin
            Q_max = Q_max.max(axis=1) * 0.5

            

            self.flow_DSO_def = self.flow[self.l_idx_dso] == self.B_dso.values * self.theta - self.G_dso.values * self.V
            self.q_flow_DSO_def = self.q_flow == self.B_star.values * self.V + self.G_dso.values * self.theta - self.b_zero.values
            self.q_pow_bal_dso = -self.I_ag.loc[self.n_idx_dso][
                self.ag_idx_dso].values * self.Q + self.I_fl_dso.values * self.q_flow == 0

            if self.grid:
                self.theta_UB = self.theta <= 2 * pd.np.pi
                self.theta_LB = self.theta >= -2 * pd.np.pi
                self.Q_UB = self.Q <= Q_max.values
                self.Q_LB = self.Q >= -Q_max.values
                self.V_UB = self.V <= self.nodes.loc[self.n_idx_dso].Vmax.values
                self.V_LB = self.V >= self.nodes.loc[self.n_idx_dso].Vmin.values
                if self.conic:
                    self.q_flow_B = []
                    for l in range(len(self.l_idx_dso)):
                        self.q_flow_B += [
                        cvx.square(self.flow[l]) + cvx.square(self.q_flow[l]) <= self.lines.loc[l].cap **2
                        ]
                else:
                    self.q_flow_B = [
                        self.flow[self.l_idx_dso] <= self.lines.loc[self.l_idx_dso].cap.values,
                        self.flow[self.l_idx_dso] >= -self.lines.loc[self.l_idx_dso].cap.values
                    ]
                self.DSO_constraints = [
                                        self.theta_UB, self.theta_LB, self.Q_UB, self.Q_LB, self.V_UB, self.V_LB,
                                        self.flow_DSO_def, self.q_flow_DSO_def, self.pow_bal_dso, self.q_pow_bal_dso
                                    ] + self.q_flow_B + self.theta_ref_DSO
            else:
                self.DSO_constraints = [
                                        self.flow_DSO_def, self.q_flow_DSO_def, self.pow_bal_dso, self.q_pow_bal_dso
                                    ] + self.theta_ref_DSO
        else:
            self.flow_DSO_def = self.flow[self.l_idx_dso] == self.B_dso.values * self.theta
            if self.grid:
                self.theta_UB = self.theta <= 2 * pd.np.pi
                self.theta_LB = self.theta >= -2 * pd.np.pi 
                self.q_flow_B = [
                    self.flow[self.l_idx_dso] <= self.lines.loc[self.l_idx_dso].cap.values,
                    self.flow[self.l_idx_dso] >= -self.lines.loc[self.l_idx_dso].cap.values
                ]
                self.DSO_constraints = [
                                        self.theta_UB, self.theta_LB, self.flow_DSO_def, self.pow_bal_dso
                                    ] + + self.q_flow_B + self.theta_ref_DSO
            else:
                self.DSO_constraints = [
                                        self.flow_DSO_def, self.pow_bal_dso
                                    ] + self.theta_ref_DSO


    def create_losses_constraints(self, N):
        # Losses constraints
        self.losses_constraints = []
        if self.loss_mod:
            self.line_loss_def = []
            for l in self.lines.index:
                if abs(self.LF_AC.loc[l]).max().max() == 0:
                    self.line_loss_def += [self.line_loss[l] == 0]
                else:
                    for col in self.LF_AC.columns:
                        self.line_loss_def += [self.line_loss[l] >= 0]
                        self.line_loss_def += [self.line_loss[l] >= self.LF_AC.loc[l].at['c', col] +
                                               self.LF_AC.loc[l].at['m', col] * self.flow[l]]
                        self.line_loss_def += [self.line_loss[l] >= self.LF_AC.loc[l].at['c', col] -
                                               self.LF_AC.loc[l].at['m', col] * self.flow[l]]

            for l in self.lines_HVDC.index:
                for col in self.LF_HVDC.columns:
                    self.line_loss_def += [self.line_loss_HVDC[l] >= 0]
                    self.line_loss_def += [
                        self.line_loss_HVDC[l] >= self.LF_HVDC.loc[l].at['c', col] +
                        self.LF_HVDC.loc[l].at['m', col] * self.flow_HVDC[l]]
                    self.line_loss_def += [
                        self.line_loss_HVDC[l] >= self.LF_HVDC.loc[l].at['c', col] -
                        self.LF_HVDC.loc[l].at['m', col] * self.flow_HVDC[l]]

            self.A_i, self.A_j = self.create_allocation(N)
            # import ipdb; ipdb.set_trace()
            self.loss_alloc = [
                self.Wag_i == 0.5 * (self.A_i.values * self.line_loss + self.A_i.values[:,0] * self.line_loss_HVDC),    # TO DO: add HVDC losses
                self.Wag_j == 0.5 * (self.A_j.values * self.line_loss + self.A_j.values[:,0] * self.line_loss_HVDC),
            ]

            # flows_to_nodes = self.flows_to_nodes_tso.append(self.flows_to_nodes_dso).T
            # self.node_loss_def = self.node_loss == flows_to_nodes.values * self.line_loss + cvx.reshape(
            #     self.flows_to_nodes_tso_HVDC.T.values * self.line_loss_HVDC, (len(self.nodes),))

            # self.agent_loss_def = self.Zag == self.nodes_to_agents.values * self.node_loss


            self.losses_constraints = self.loss_alloc + self.line_loss_def
                                    # [
                                    #     self.node_loss_def, self.agent_loss_def
                                    # ] + self.line_loss_def

        
        

    def create_B(self, N):
        fbus_TSO = self.lines.loc[self.l_idx_tso].fbus.values.astype(int)
        tbus_TSO = self.lines.loc[self.l_idx_tso].tbus.values.astype(int)
        B = pd.DataFrame(0., index=self.l_idx_tso, columns=self.n_idx_tso)
        for i in range(len(self.l_idx_tso)):
            B.at[self.l_idx_tso[i], fbus_TSO[i]] = 1 / self.lines.loc[self.l_idx_tso[i]].x
            B.at[self.l_idx_tso[i], tbus_TSO[i]] = -1 / self.lines.loc[self.l_idx_tso[i]].x

        return B

    def create_PTDF(self, N):
        Bp = pd.np.diag(1 / self.lines.loc[self.l_idx_tso].x)
        Bbus = pd.DataFrame(pd.np.dot(self.I_fl.values, pd.np.dot(Bp, self.I_fl.T.values)), index=self.n_idx_tso,
                            columns=self.n_idx_tso)
        idx = self.nodes[(self.nodes.type == 3) & (self.nodes.ID == 'T1')].index
        idx_HVDC = []
        bus = self.lines.fbus.append(self.lines.tbus).values
        for i in self.n_idx_tso:
            if i not in bus:
                idx_HVDC += [i]
        Bstar = Bbus.copy().drop(idx.tolist()+idx_HVDC).drop(idx.tolist()+idx_HVDC, axis=1)
        Binv = pd.DataFrame(pd.np.linalg.inv(Bstar.values), index=self.n_idx_tso.drop(idx.tolist()+idx_HVDC),
                            columns=self.n_idx_tso.drop(idx.tolist()+idx_HVDC))
        Bbar = pd.DataFrame(0., index=self.n_idx_tso, columns=self.n_idx_tso)
        Bbar = (Bbar + Binv).fillna(0)
        PTDF = pd.DataFrame(pd.np.dot(self.B.values, Bbar.values), index=self.l_idx_tso, columns=self.n_idx_tso)
        return PTDF

    def create_allocation(self, N):
        A_i = pd.DataFrame(0., index=range(len(self.partner_i)), columns=self.lines.index)
        for i in range(len(self.partner_i)): 
            A_i.loc[i] = self.agents.iloc[self.partner_i[i]].K / self.agents.K.sum() #1/len(self.partner_i) #

        A_j = pd.DataFrame(0., index=range(len(self.partner_j)), columns=self.lines.index)
        for j in range(len(self.partner_j)):
            A_j.loc[j] = self.agents.iloc[self.partner_j[j]].K / self.agents.K.sum() #1/len(self.partner_j) #

        return A_i, A_j


    def create_PTDF_ex(self, N):
        Bp = pd.np.diag(1 / self.lines.loc[self.l_idx_tso].x)
        Bbus = pd.DataFrame(pd.np.dot(self.I_fl.values, pd.np.dot(Bp, self.I_fl.T.values)), index=self.n_idx_tso,
                            columns=self.n_idx_tso)
        idx = self.nodes[(self.nodes.type == 3) & (self.nodes.ID == 'T1')].index
        idx_HVDC = []
        bus = self.lines.fbus.append(self.lines.tbus).values
        for i in self.n_idx_tso:
            if i not in bus:
                idx_HVDC += [i]
        Bstar = Bbus.copy().drop(idx.tolist()+idx_HVDC).drop(idx.tolist()+idx_HVDC, axis=1)
        Binv = pd.DataFrame(pd.np.linalg.inv(Bstar.values), index=self.n_idx_tso.drop(idx.tolist()+idx_HVDC),
                            columns=self.n_idx_tso.drop(idx.tolist()+idx_HVDC))
        Bbar = pd.DataFrame(0., index=self.n_idx_tso, columns=self.n_idx_tso)
        Bbar = (Bbar + Binv).fillna(0)
        PTDF = pd.DataFrame(pd.np.dot(self.B.values, Bbar.values), index=self.l_idx_tso, columns=self.n_idx_tso)
        return PTDF

    def create_incidence(self, N):
        I_ag = pd.DataFrame(0., index=self.nodes.index, columns=self.agents.index)
        for a in self.agents.index:
            bus = self.agents.loc[a].bus
            if not pd.isna(bus):
                try:
                    I_ag.at[bus, a] = 1
                except:
                    import ipdb; ipdb.set_trace()

        I_fl = self.B.copy().T
        I_fl[I_fl > 0] = 1
        I_fl[I_fl < 0] = -1

        fbus = self.lines_HVDC.fbus.values.astype(int)
        tbus = self.lines_HVDC.tbus.values.astype(int)
        I_fl_HVDC = pd.DataFrame(0., index=self.n_idx_tso, columns=self.lines_HVDC.index)
        for i in range(len(self.lines_HVDC)):
            I_fl_HVDC.at[fbus[i], self.lines_HVDC.index[i]] = 1
            I_fl_HVDC.at[tbus[i], self.lines_HVDC.index[i]] = -1

        I_td = pd.DataFrame(0., index=self.nodes.index, columns=N.TSO_DSO.keys())
        for k in N.TSO_DSO.keys():
            I_td.at[N.TSO_DSO[k], k] = 1
            I_td.at[self.nodes[(self.nodes.type == 3) & (self.nodes.ID == 'D' + str(k))].index, k] = -1

        return I_ag, I_fl, I_fl_HVDC, I_td

    def create_B_dso(self, N):
        Y = -1 / (self.lines.r + 1j * self.lines.x)
        B_bus = pd.Series(Y.imag, index=Y.index)
        G_bus = pd.Series(Y.real, index=Y.index)
        fbus_DSO = pd.Series(self.lines.fbus.values.astype(int), index=self.lines.index)
        tbus_DSO = pd.Series(self.lines.tbus.values.astype(int), index=self.lines.index)
        B_dso = pd.DataFrame(0., index=self.l_idx_dso, columns=self.n_idx_dso)
        G_dso = pd.DataFrame(0., index=self.l_idx_dso, columns=self.n_idx_dso)
        B_star = pd.DataFrame(0., index=self.l_idx_dso, columns=self.n_idx_dso)
        b_zero = pd.Series(0., index=self.l_idx_dso)
        for i in self.l_idx_dso:
            B_dso.at[i, fbus_DSO.loc[i]] = B_bus[i]
            B_dso.at[i, tbus_DSO.loc[i]] = -B_bus[i]

            G_dso.at[i, fbus_DSO.loc[i]] = -G_bus[i]
            G_dso.at[i, tbus_DSO.loc[i]] = G_bus[i]

            B_star.at[i, fbus_DSO.loc[i]] = B_bus[i] + self.lines.loc[i].b
            B_star.at[i, tbus_DSO.loc[i]] = -B_bus[i]

            b_zero.loc[i] = 0.5 * self.lines.loc[i].b

        return B_dso, G_dso, B_star, b_zero

    def create_incidence_dso(self, N):
        I_fl = self.B_dso.copy().T
        I_fl[I_fl > 0] = 1
        I_fl[I_fl < 0] = -1

        return I_fl

    def solve(self, v):
        self.cvx_problem = cvx.Problem(cvx.Minimize(self.obj_ag), #+ 0.01*cvx.norm(self.flow_HVDC, 1) ),
                                       self.ag_constraints + self.TSO_constraints + self.DSO_constraints
                                       + self.losses_constraints)
        self.cvx_problem.solve(solver=cvx.MOSEK, verbose=v)

        # list_solv = cvx.installed_solvers()
        # check_dual = True
        # count = 0
        # while check_dual:
        #     try:
        #         self.cvx_problem.solve(solver=eval('cvx.'+cvx.installed_solvers()[count]), verbose=v)
        #         if max(abs(self.ag_pow_bal.dual_value/Sb)) > 0.001:
        #             print('Solver used:', list_solv[count])
        #             check_dual = False
        #         else:
        #             if count == len(list_solv):
        #                 print('ERROR: Tried all solvers')
        #                 check_dual = False
        #             else:
        #                 count += 1
        #     except:
        #         if count == len(list_solv):
        #             print('ERROR: Tried all solvers')
        #             check_dual = False
        #         else:
        #             count += 1
