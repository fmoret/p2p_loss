import numpy as np
import pandas as pd


class Results:
    def __init__(self, prob, N):
        Sb = N.TSOs[1].Sb
        X = pd.DataFrame(index=prob.agents.index, columns=['P', 'Q', 'W', 'Z', 'Pi', 'UB_p', 'LB_p', 'Loss_pi']) #, 'LS'
        X.P = prob.Pag.value * Sb
        if prob.react and prob.DSO:
            X.at[prob.ag_idx_dso, 'Q'] = prob.Q.value * Sb
        # import ipdb; ipdb.set_trace()
        X.Pi = [prob.ag_pow_bal[i].dual_value / Sb for i in range(len(prob.agents))]
        #X.LS = prob.Ls.value * Sb

        X.UB_p = prob.ag_UB.dual_value / Sb
        X.LB_p = prob.ag_LB.dual_value / Sb

        if prob.loss_mod and prob.grid:
            df = pd.DataFrame([prob.partner_i, prob.Wag_i.value], index=['ID', 'loss']).T.append(pd.DataFrame([prob.partner_j, prob.Wag_j.value], index=['ID', 'loss']).T)
            X.W = df.groupby(['ID']).sum().values *Sb

        df = pd.DataFrame([prob.partner_i, prob.z_i.value], index=['ID', 'loss']).T.append(pd.DataFrame([prob.partner_j, prob.z_j.value], index=['ID', 'loss']).T)
        X.Z = df.groupby(['ID']).sum().values *Sb

        self.Agents = X

        self.Trades = pd.DataFrame(index=range(len(prob.agents)), columns=range(len(prob.agents)))
        self.Trades_prices = pd.DataFrame(index=range(len(prob.agents)), columns=range(len(prob.agents)))
        for i in range(len(prob.partner_i)):
            self.Trades.at[prob.partner_i[i], prob.partner_j[i]] = prob.Tag_i[i].value * Sb
            self.Trades.at[prob.partner_j[i], prob.partner_i[i]] = prob.Tag_j[i].value * Sb

            self.Trades_prices.at[prob.partner_i[i], prob.partner_j[i]] = prob.trades_rec.dual_value[i] / Sb
            self.Trades_prices.at[prob.partner_j[i], prob.partner_i[i]] = prob.trades_rec.dual_value[i] / Sb

        if prob.loss_mod and prob.grid:
            self.Grid_prices = pd.DataFrame(index=range(len(prob.agents)), columns=range(len(prob.agents)))
            for i in range(len(prob.partner_i)):
                self.Grid_prices.at[prob.partner_i[i], prob.partner_j[i]] = prob.loss_trade[0].dual_value[i]/Sb
                self.Grid_prices.at[prob.partner_j[i], prob.partner_i[i]] = prob.loss_trade[1].dual_value[i]/Sb

        self.Trades.index = prob.agents.index
        self.Trades.columns = prob.agents.index
        self.Trades_prices.index = prob.agents.index
        self.Trades_prices.columns = prob.agents.index

        self.Flows = pd.DataFrame(index=prob.nodes.index, columns=prob.nodes.index)
        flow = prob.flow.value * Sb
        for l in prob.lines.index:
            self.Flows.at[prob.lines.loc[l].fbus, prob.lines.loc[l].tbus] = flow[l]
            self.Flows.at[prob.lines.loc[l].tbus, prob.lines.loc[l].fbus] = -flow[l]
        
        if prob.HVDC:
            flow_HVDC = prob.flow_HVDC.value * Sb
            for l in prob.lines_HVDC.index:
                self.Flows.at[prob.lines_HVDC.loc[l].fbus, prob.lines_HVDC.loc[l].tbus] = flow_HVDC[l]
                self.Flows.at[prob.lines_HVDC.loc[l].tbus, prob.lines_HVDC.loc[l].fbus] = -flow_HVDC[l]

        if prob.DSO:
            count = 0
            flow_td = prob.flow_TD.value * Sb
            for k in N.TSO_DSO.keys():
                self.Flows.at[N.TSO_DSO[k], prob.nodes[(prob.nodes.type == 3) & (prob.nodes.ID == 'D' + str(k))].index] = \
                flow_td[count]
                self.Flows.at[prob.nodes[(prob.nodes.type == 3) & (prob.nodes.ID == 'D' + str(k))].index, N.TSO_DSO[k]] = - \
                flow_td[count]
                count += 1

            if prob.react:
                self.Q_Flows = pd.DataFrame(index=prob.nodes.index, columns=prob.nodes.index)
                q_flow = prob.q_flow.value * Sb
                count = 0
                for l in prob.l_idx_dso:
                    self.Q_Flows.at[prob.lines.loc[l].fbus, prob.lines.loc[l].tbus] = q_flow[count]
                    self.Q_Flows.at[prob.lines.loc[l].tbus, prob.lines.loc[l].fbus] = -q_flow[count]
                    count += 1

                self.V = pd.Series(prob.V.value, index=prob.n_idx_dso)

            self.Theta = pd.Series(prob.theta.value, index=prob.n_idx_dso)
        
        # self.Nodal = pd.Series(index=prob.nodes.index)
        # if prob.DSO:
        #     self.Nodal.loc[prob.n_idx_dso] = prob.pow_bal_dso.dual_value / Sb
        
        self.pow_bal_tso = (pd.np.dot(-prob.I_ag.loc[prob.n_idx_tso].values, prob.Pag.value) + pd.np.dot(prob.I_fl.values, prob.flow[prob.l_idx_tso].value))*Sb #+ prob.Ls.value
        if prob.DSO:
            self.pow_bal_tso += pd.np.dot(prob.I_td.loc[prob.n_idx_tso].values, prob.flow_DT.value) * Sb
            self.pow_bal_dso = (pd.np.dot(-prob.I_ag.loc[prob.n_idx_dso].values, prob.Pag.value) + pd.np.dot(prob.I_fl_dso.values, prob.flow[prob.l_idx_dso].value)
                                +pd.np.dot(prob.I_td.loc[prob.n_idx_dso].values, prob.flow_TD.value))*Sb 
        if prob.loss_mod:
            self.pow_bal_tso += pd.np.dot(prob.flows_to_nodes_tso[prob.n_idx_tso].values.T, prob.line_loss[prob.l_idx_tso].value)*Sb
            if prob.DSO:
                self.pow_bal_dso += pd.np.dot(prob.flows_to_nodes_dso[prob.n_idx_dso].values.T, prob.line_loss[prob.l_idx_dso].value)*Sb 
        if prob.HVDC:
            self.pow_bal_tso += pd.np.dot(prob.I_fl_HVDC.values, prob.flow_HVDC.value)*Sb
            if prob.loss_mod:
                self.pow_bal_tso += pd.np.dot(prob.flows_to_nodes_tso_HVDC[prob.n_idx_tso].values.T, prob.line_loss_HVDC.value)*Sb
            
        self.pow_bal_dso = pd.Series(self.pow_bal_dso, index=prob.n_idx_dso)
        self.pow_bal_tso = pd.Series(self.pow_bal_tso, index=prob.n_idx_tso)

        
        #%% Network data
        self.social_TSOs = []
        self.social_DSOs = []
        
        self.case = N.case
        self.loss = prob.loss_mod
        self.grid = prob.grid
        for t in N.TSOs:
            self.social_TSOs.append(N.TSOs[t].social)
        for d in N.DSOs:
            self.social_DSOs.append(N.DSOs[d].social)
        
        self.social_TSOs = pd.DataFrame(data=self.social_TSOs)
        self.social_DSOs = pd.DataFrame(data=self.social_DSOs)
        
        self.N_agents       = prob.agents
        self.Communication  = prob.communication
        self.N_nodes        = prob.nodes
        self.N_nodes_dso    = prob.nodes_dso
        self.N_lines        = prob.lines
        self.N_lines_HVDC   = prob.lines_HVDC
        self.TSO_DSO        = pd.DataFrame(data=[k for k in N.TSO_DSO.values()])
        
        # PTDF distances estimation - Between nodes
        TSO_nodes = prob.n_idx_tso.tolist()
        Nodes_distances = pd.DataFrame(data=np.zeros([len(TSO_nodes),len(TSO_nodes)]))
        Nodes_distances.index = TSO_nodes
        Nodes_distances.columns = TSO_nodes
        for i in TSO_nodes:
            for j in TSO_nodes:
                Nodes_distances[i][j] = sum(prob.PTDF[:][i])-sum(prob.PTDF[:][j])
        
        # PTDF distances estimation - Between agents
        TSO_peers = prob.ag_idx_tso.tolist()
        TSO_CMs = prob.agents[prob.agents.CM].index.tolist()
        TSO_peers.extend(TSO_CMs)
        TSO_peer_buses = prob.agents.bus[TSO_peers].copy()
        for k in N.TSO_DSO.keys():
            TSO_peer_buses[TSO_CMs[k-1]] = N.TSO_DSO[k]-1
        self.Agents_distances = pd.DataFrame(data=np.zeros([len(TSO_peers),len(TSO_peers)]))
        self.Agents_distances.index = TSO_peers
        self.Agents_distances.columns = TSO_peers
        for n in TSO_peers:
            for m in TSO_peers:
                self.Agents_distances[n][m] = Nodes_distances[TSO_peer_buses[n]][TSO_peer_buses[m]]
        self.Agents_distances.index = [str(i) for i in TSO_peers]
        self.Agents_distances.columns = [str(i) for i in TSO_peers]
        
        # Extract trades at TSO level only
        self.Trades_TSO = self.Trades.loc[TSO_peers][TSO_peers]
        self.Trades_prices_TSO = self.Trades_prices.loc[TSO_peers][TSO_peers]
        
        
        
        