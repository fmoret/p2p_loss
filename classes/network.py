import scipy.io as spio
import numpy as np
import pandas as pd
import pickle

class Tso:
    def __init__(self, TG, ID_ag, ID_bus, ID_line, alpha=0.5, social=0):
        self.ID_ag = ID_ag
        self.ID_bus = ID_bus
        self.ID_line = ID_line

        self.alpha = alpha
        self.social = social

        for key in TG:
            if key == 'baseMVA':
                name = 'Sb'
                data = TG[key]
                setattr(self, name, data)
            elif key == 'bus':
                name = key
                cols = ['loc_bus', 'type', 'Pd', 'Qd', 'Gs', 'Bs', 'area', 'Vm', 'Va', 'baseKV', 'zone', 'Vmax',
                        'Vmin']  # , 'x1', 'x2']
                arr = np.array(TG[key])[:, :13]
                idx = np.arange(arr.shape[0]) + self.ID_bus
                self.ID_bus = max(idx) + 1
                data = pd.DataFrame(index=idx, data=arr, columns=cols)
                data.index = data.index.astype(int)
                setattr(self, name, data)
            elif key == 'gen':
                name = key
                cols = ['loc_bus', 'Pg', 'Qg', 'Qmax', 'Qmin', 'Vg', 'mBase', 'status', 'Pmax', 'Pmin', 'Pc1', 'Pc2',
                        'Qc1min', 'Qc1max', 'Qc2min', 'Qc2max', 'ramp_agc', 'ramp_10', 'ramp_30', 'ramp_q', 'apf']
                arr = np.array(TG[key])
                idx = np.arange(arr.shape[0]) + self.ID_ag
                self.ID_ag = max(idx) + 1
                data = pd.DataFrame(index=idx, data=arr, columns=cols)
                data.index = data.index.astype(int)
                setattr(self, name, data)
                for i in self.gen.index:
                    self.gen.at[i, 'bus'] = int(self.bus[self.bus.loc_bus == self.gen.at[i, 'loc_bus']].index.values[0])
            elif key == 'load':
                name = key
                cols = ['loc_bus', 'Pmax', 'Pmin', 'xxx']
                arr = np.array(TG[key])
                idx = np.arange(arr.shape[0]) + self.ID_ag
                self.ID_ag = max(idx) + 1
                data = pd.DataFrame(index=idx, data=arr, columns=cols)
                data.index = data.index.astype(int)
                data.Pmax *= -1
                data.Pmin *= -1
                setattr(self, name, data)
                for i in self.load.index:
                    self.load.at[i, 'bus'] = int(
                        self.bus[self.bus.loc_bus == self.load.at[i, 'loc_bus']].index.values[0])
            elif key == 'branch':
                name = key
                cols = ['loc_fbus', 'loc_tbus', 'r', 'x', 'b', 'rateA', 'rateB', 'rateC', 'ratio', 'angle', 'status',
                        'angmin', 'angmax']
                arr = np.array(TG[key])
                try:
                    j = arr.shape[1]
                    idx = np.arange(arr.shape[0]) + self.ID_line
                    self.ID_line = max(idx) + 1
                except:
                    idx = [self.ID_line]
                    self.ID_line += 1
                try:
                    data = pd.DataFrame(index=idx, data=arr, columns=cols)
                except:
                    data = pd.DataFrame(index=idx, data=[arr], columns=cols)
                setattr(self, name, data)
                for i in self.branch.index:
                    self.branch.at[i, 'fbus'] = int(
                        self.bus[self.bus.loc_bus == self.branch.at[i, 'loc_fbus']].index.values[0])
                    self.branch.at[i, 'tbus'] = int(
                        self.bus[self.bus.loc_bus == self.branch.at[i, 'loc_tbus']].index.values[0])
            elif key == 'branchHVDC':
                name = key
                cols = ['loc_fbus', 'loc_tbus', 'r', 'rateA', 'x1', 'x2', 'x3', 'x4']
                arr = np.array(TG[key])
                try:
                    j = arr.shape[1]
                    idx = np.arange(arr.shape[0]) + self.ID_line
                    self.ID_line = max(idx) + 1
                except:
                    idx = [self.ID_line]
                    self.ID_line += 1
                try:
                    data = pd.DataFrame(index=idx, data=arr, columns=cols)
                except:
                    data = pd.DataFrame(index=idx, data=[arr], columns=cols)
                setattr(self, name, data)
                for i in self.branchHVDC.index:
                    self.branchHVDC.at[i, 'fbus'] = int(
                        self.bus[self.bus.loc_bus == self.branchHVDC.at[i, 'loc_fbus']].index.values[0])
                    self.branchHVDC.at[i, 'tbus'] = int(
                        self.bus[self.bus.loc_bus == self.branchHVDC.at[i, 'loc_tbus']].index.values[0])
            elif key == 'gencost':
                cols = ['startup', 'shutdown', 'n', 'lin', 'quad']
                arr = np.array(TG[key])
                data = pd.DataFrame(index=self.gen.index, data=arr[:, 1:], columns=cols)
                self.gen = self.gen.join(data)
            elif key == 'loadutil':
                cols = ['n', 'lin', 'quad']
                arr = np.array(TG[key])
                data = pd.DataFrame(index=self.load.index, data=arr[:, 1:], columns=cols)
                self.load = self.load.join(data)
            elif key == 'comm':
                self.comm = TG[key]

        self.gen.drop(
            columns=['Pg', 'Qg', 'Qmax', 'Qmin', 'Vg', 'mBase', 'status', 'Pc1', 'Pc2', 'Qc1min', 'Qc1max', 'Qc2min',
                     'Qc2max', 'ramp_agc', 'ramp_10', 'ramp_30', 'ramp_q', 'apf', 'startup', 'shutdown', 'n'],
            inplace=True)
        self.load.drop(columns=['xxx', 'n'], inplace=True)
        self.agent = self.gen.append(self.load)

        # Change in p.u.
        self.agent.Pmax = self.agent.Pmax / self.Sb
        self.agent.Pmin = self.agent.Pmin / self.Sb
        self.agent.lin = self.agent.lin.astype(float) * self.Sb
        self.agent.quad = self.agent.quad.astype(float) * (self.Sb ^ 2)

        self.branch.rateA = self.branch.rateA / self.Sb
        self.branchHVDC.rateA = self.branchHVDC.rateA / self.Sb


class Dso:
    def __init__(self, TG, ID_ag, ID_bus, ID_line, alpha=0, social=1):
        self.ID_ag = ID_ag
        self.ID_bus = ID_bus
        self.ID_line = ID_line

        self.alpha = alpha
        self.social = social

        for key in TG:
            if key == 'baseMVA':
                name = 'Sb'
                data = TG[key]
                setattr(self, name, data)
            elif key == 'bus':
                name = key
                cols = ['loc_bus', 'type', 'Pd', 'Qd', 'Gs', 'Bs', 'area', 'Vm', 'Va', 'baseKV', 'zone', 'Vmax',
                        'Vmin']  # , 'x1', 'x2']
                arr = np.array(TG[key])[:, :13]
                idx = np.arange(arr.shape[0]) + self.ID_bus
                self.ID_bus = max(idx) + 1
                data = pd.DataFrame(index=idx, data=arr, columns=cols)
                data.index = data.index.astype(int)
                setattr(self, name, data)
            elif key == 'agent':
                name = key
                cols = ['loc_bus', 'Pmax', 'Pmin']
                arr = np.array(TG[key])
                idx = np.arange(arr.shape[0]) + self.ID_ag
                self.ID_ag = max(idx) + 2
                data = pd.DataFrame(index=idx, data=arr, columns=cols)
                data.index = data.index.astype(int)
                setattr(self, name, data)
                for i in self.agent.index:
                    self.agent.at[i, 'bus'] = int(
                        self.bus[self.bus.loc_bus == self.agent.at[i, 'loc_bus']].index.values[0])

            elif key == 'branch':
                name = key
                cols = ['loc_fbus', 'loc_tbus', 'r', 'x', 'b', 'rateA', 'rateB', 'rateC', 'ratio', 'angle', 'status',
                        'angmin', 'angmax']
                arr = np.array(TG[key])
                idx = np.arange(arr.shape[0]) + self.ID_line
                self.ID_line = max(idx) + 1
                try:
                    data = pd.DataFrame(index=idx, data=arr, columns=cols)
                except:
                    data = pd.DataFrame(index=idx, data=[arr], columns=cols)
                setattr(self, name, data)
                for i in self.branch.index:
                    self.branch.at[i, 'fbus'] = int(
                        self.bus[self.bus.loc_bus == self.branch.at[i, 'loc_fbus']].index.values[0])
                    self.branch.at[i, 'tbus'] = int(
                        self.bus[self.bus.loc_bus == self.branch.at[i, 'loc_tbus']].index.values[0])

            elif key == 'agcost':
                cols = ['startup', 'shutdown', 'n', 'lin', 'quad']
                arr = np.array(TG[key])
                data = pd.DataFrame(index=self.agent.index, data=arr[:, 1:], columns=cols)
                self.agent = self.agent.join(data)
                self.agent.quad = self.agent.quad / 100

        self.agent.drop(columns=['startup', 'shutdown', 'n'], inplace=True)

        # Change in p.u.
        self.agent.Pmax = self.agent.Pmax / self.Sb
        self.agent.Pmin = self.agent.Pmin / self.Sb
        self.agent.lin = self.agent.lin.astype(float) * self.Sb
        self.agent.quad = self.agent.quad.astype(float) * (self.Sb ^ 2)

        # self.agent.at[6,'Pmax'] = 0.01
        # import ipdb; ipdb.set_trace()
        self.branch.rateA = self.branch.rateA / self.Sb


class Network:
    def __init__(self, case='small', alpha_TSO=None, social_TSO=None, alpha_DSO=None, social_DSO=None, DSO=True):
        ID_ag = 0
        ID_bus = 0
        ID_line = 0
        self.case = case
        if case == 'small':
            TG = self.loadmat('mpc_TG.mat')['mpc_TG']
            DG = self.loadmat('mpc_DG.mat')['mpc_DG']
        elif case == 'big':
            TG = self.loadmat('mpc_TG_big.mat')['ans']
            DG = self.loadmat('mpc_DG_big.mat')['mpc_DG']
        elif case == '2':
            TG = self.loadmat('mpc_TG_2.mat')['ans']
            DG = self.loadmat('mpc_DG_2.mat')['ans']
        elif case == 'final':
            TG = self.loadmat('case96_TG.mat')['case96_TG']
            DG = pickle.load( open( "DSO.pkl", "rb" ) )

        if alpha_TSO is None or social_TSO is None:
            self.TSOs = {
                1: Tso(TG, ID_ag, ID_bus, ID_line)
                }
        else:
            self.TSOs = {
                1: Tso(TG, ID_ag, ID_bus, ID_line, alpha=alpha_TSO, social=social_TSO)
                }

        community_links = self.TSOs[1].comm
        if type(community_links) is not list:
            community_links = [community_links]
        if not DSO:
            community_links = []

        self.DSOs = {}
        self.TSO_DSO = {}
        count = 1
        for c in community_links:
            if count==1:
                id_ag = self.TSOs[1].ID_ag
                id_bus = self.TSOs[1].ID_bus
                id_line = self.TSOs[1].ID_line
            else:
                id_ag = self.DSOs[count-1].ID_ag
                id_bus = self.DSOs[count-1].ID_bus
                id_line = self.DSOs[count-1].ID_line

            if alpha_DSO is None or social_DSO is None:
                if case == 'final':
                    self.DSOs[count] = Dso(DG[count-1], id_ag, id_bus, id_line)
                else:
                    self.DSOs[count] = Dso(DG, id_ag, id_bus, id_line)
            else:
                if case == 'final':
                    self.DSOs[count] = Dso(DG[count-1], id_ag, id_bus, id_line, alpha=alpha_DSO, social=social_DSO)
                else:
                    self.DSOs[count] = Dso(DG, id_ag, id_bus, id_line, alpha=alpha_DSO, social=social_DSO)
            self.TSO_DSO[count] = c - 1
            count += 1

        self.Sb = self.TSOs[1].Sb

    def loadmat(self, filename):
        '''
        this function should be called instead of direct spio.loadmat
        as it cures the problem of not properly recovering python dictionaries
        from mat files. It calls the function check keys to cure all entries
        which are still mat-objects
        '''

        def _check_keys(d):
            '''
            checks if entries in dictionary are mat-objects. If yes
            todict is called to change them to nested dictionaries
            '''
            for key in d:
                if isinstance(d[key], spio.matlab.mio5_params.mat_struct):
                    d[key] = _todict(d[key])
            return d

        def _todict(matobj):
            '''
            A recursive function which constructs from matobjects nested dictionaries
            '''
            d = {}
            for strg in matobj._fieldnames:
                elem = matobj.__dict__[strg]
                if isinstance(elem, spio.matlab.mio5_params.mat_struct):
                    d[strg] = _todict(elem)
                elif isinstance(elem, np.ndarray):
                    d[strg] = _tolist(elem)
                else:
                    d[strg] = elem
            return d

        def _tolist(ndarray):
            '''
            A recursive function which constructs lists from cellarrays
            (which are loaded as numpy ndarrays), recursing into the elements
            if they contain matobjects.
            '''
            elem_list = []
            for sub_elem in ndarray:
                if isinstance(sub_elem, spio.matlab.mio5_params.mat_struct):
                    elem_list.append(_todict(sub_elem))
                elif isinstance(sub_elem, np.ndarray):
                    elem_list.append(_tolist(sub_elem))
                else:
                    elem_list.append(sub_elem)
            return elem_list

        data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
        return _check_keys(data)
