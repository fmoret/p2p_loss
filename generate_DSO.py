import pandas as pd 
import scipy.io as spio
import numpy as np
import pickle

def loadmat(filename):
    '''
    this function should be called instead of direct spio.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects
    '''

    def _check_keys(d):
        '''
        checks if entries in dictionary are mat-objects. If yes
        todict is called to change them to nested dictionaries
        '''
        for key in d:
            if isinstance(d[key], spio.matlab.mio5_params.mat_struct):
                d[key] = _todict(d[key])
        return d

    def _todict(matobj):
        '''
        A recursive function which constructs from matobjects nested dictionaries
        '''
        d = {}
        for strg in matobj._fieldnames:
            elem = matobj.__dict__[strg]
            if isinstance(elem, spio.matlab.mio5_params.mat_struct):
                d[strg] = _todict(elem)
            elif isinstance(elem, np.ndarray):
                d[strg] = _tolist(elem)
            else:
                d[strg] = elem
        return d

    def _tolist(ndarray):
        '''
        A recursive function which constructs lists from cellarrays
        (which are loaded as numpy ndarrays), recursing into the elements
        if they contain matobjects.
        '''
        elem_list = []
        for sub_elem in ndarray:
            if isinstance(sub_elem, spio.matlab.mio5_params.mat_struct):
                elem_list.append(_todict(sub_elem))
            elif isinstance(sub_elem, np.ndarray):
                elem_list.append(_tolist(sub_elem))
            else:
                elem_list.append(sub_elem)
        return elem_list

    data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)



DG = loadmat('DSO_33.mat')['ans']

capacity = np.array([
    [4600, 4600],
    [4100,	4100],
    [2900,	2900],
    [2900,	2900],
    [2900,	2900],
    [1500,	1500],
    [1050,	1050],
    [1050,	1050],
    [1050,	1050],
    [1050,	1050],
    [1050,	1050],
    [500,	500],
    [450,	450],
    [300,	300],
    [250,	250],
    [250,	250],
    [100,	100],
    [500,	500],
    [500,	500],
    [210,	210],
    [110,	110],
    [1050,	1050],
    [1050,	1050],
    [500,	500],
    [1500,	1500],
    [1500,	1500],
    [1500,	1500],
    [1500,	1500],
    [1500,	1500],
    [500,	500],
    [500,	500],
    [100,	100]




    # [0.1000, 0.0600],
    # [0.0900, 0.0400],
    # [0.1200, 0.0800],
    # [0.0600, 0.0300],
    # [0.0600, 0.0200],
    # [0.2000, 0.1000],
    # [0.2000, 0.1000],
    # [0.0600, 0.0200],
    # [0.0600, 0.0200],
    # [0.0450, 0.0300],
    # [0.0600, 0.0350],
    # [0.0600, 0.0350],
    # [0.1200, 0.0800],
    # [0.0600, 0.0100],
    # [0.0600, 0.0200],
    # [0.0600, 0.0200],
    # [0.0900, 0.0400],
    # [0.0900, 0.0400],
    # [0.0900, 0.0400],
    # [0.0900, 0.0400],
    # [0.0900, 0.0400],
    # [0.0900, 0.0500],
    # [0.4200, 0.2000],
    # [0.4200, 0.2000],
    # [0.0600, 0.0250],
    # [0.0600, 0.0250],
    # [0.0600, 0.0200],
    # [0.1200, 0.0700],
    # [0.2000, 0.6000],
    # [0.1500, 0.0700],
    # [0.2100, 0.1000],
    # [0.0600, 0.0400]
])/1000

capacity_real = np.sqrt(capacity[:,0]**2 + capacity[:,1]**2)

branches = [DG['branch'][b] for b in range(len(DG['branch'])-5)]
DG['branch'] = branches

for b in range(len(DG['branch'])):
    DG['branch'][b][5] = capacity_real[b]


DSO = {}

for i in range(12):
    bus_data = pd.DataFrame(DG['bus'], columns=['bus_i','type','Pd','Qd','Gs','Bs','area','Vm','Va','baseKV','zone','Vmax','Vmin'])
    bus_data.drop(bus_data[bus_data.Pd==0].index, inplace=True)
    gen_data = pd.DataFrame(0.0, index=bus_data.index, columns=['bus','Pmax','Pmin'])
    gen_data.bus = bus_data.bus_i
    np.random.seed(seed=i)
    delta = np.random.uniform(high=1.0,size=len(gen_data))
    gen_data.Pmin = -bus_data.Pd
    gen_data.Pmax = delta - bus_data.Pd

    gencost_data = pd.DataFrame(0.0, index=gen_data.index, columns=['type','startup','shutdown','n','lin','quad'])
    np.random.seed(seed=1000+i)
    gencost_data.lin = np.random.uniform(low=10.0,high=50.0,size=len(gen_data))

    DSO[i] = DG
    DSO[i]['agent'] = gen_data.values.tolist()
    DSO[i]['agcost'] = gencost_data.values.tolist()

pickle.dump( DSO, open( "DSO.pkl", "wb" ) )
import ipdb; ipdb.set_trace()

