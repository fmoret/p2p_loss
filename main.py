from classes import Network, Problem, Results
import os, scipy.io
import pandas as pd
import time
import matplotlib.pyplot as plt

# DSO = True

# N = Network(case='final', DSO=DSO)

# start = time.time()
# prob = Problem(N, verbose=True, loss=True, react=True, conic=True, grid=True, DSO=DSO, HVDC=True, cap=True)
# end = time.time()

# print(end - start)

# Res = Results(prob, N)

# import ipdb; ipdb.set_trace()


# The test case that is to be tested
testcase = 'small'
run_all = True

# Losses allocations to test
TSO_socials = [round(i*0.1,2) for i in range(11)]
DSO_socials = [round(i*0.1,2) for i in range(11)]

# Save results in .mat file
def P2P_OPFloss_save(Res,grid=None,loss=None,Tsocial=None,Dsocial=None,cap=True,ref=False):
    folder = os.getcwd() + '\\Results'
    if loss is None:
        loss=Res.loss
    if grid is None:
        grid=Res.grid
    if Tsocial is None:
        Tsocial=Res.social_TSOs[0][0]
    if Dsocial is None:
        Dsocial=Res.social_DSOs[0][0]
    if grid is False:
        res_param = f'Res{Res.case}_nogrid'
    elif loss is False:
        res_param = f'Res{Res.case}_nolosses'
    elif cap is False:
        res_param = f'Res{Res.case}_nocap'
    elif ref is True:
        res_param = f'Res{Res.case}_ref'
    else:
        res_param = f'Loop\\Res{Res.case}_Ts{Tsocial:.2f}_Ds{Dsocial:.2f}'
    filename = f'{folder}\\{res_param}.mat'
    scipy.io.savemat(filename,{key:getattr(Res,key) for key in Res.__dict__.keys()})
    # if not os.path.exists(f'{folder}\\{res_param}'):
    #     os.mkdir(f'{folder}\\{res_param}')
    # list_keys = []
    # for key in Res.__dict__.keys():
    #     if key not in ['loss','case','grid']:
    #         list_keys.append(key)
    # for key in list_keys:
    #     filename = f'{folder}\\{res_param}\\{key}.csv'
    #     getattr(Res,key).to_csv(filename)
    return

# Simulation slot
def P2P_OPFloss_simulate(N,testcase='small',verbose=True,loss=True,grid=True,autosave=True):
    prob = Problem(N, verbose=verbose, loss=loss, grid=grid)
    Res = Results(prob, N)
    if autosave:
        P2P_OPFloss_save(Res)
    return Res

# Creat network
N = Network(case=testcase)


# Reference 1 - No network constraints
# output_nogrid = P2P_OPFloss_simulate(N,testcase,verbose=False,grid=False)

# Reference 2 - No losses allocation & Losses post-estimation 
# output_noloss = P2P_OPFloss_simulate(N,testcase,verbose=False,loss=False)

if run_all:
    output = {}
    prob = Problem(N, verbose=False, loop=True)

    prob.allocation_constraints(N, s_t=0, s_d=0, cap=False)
    import ipdb; ipdb.set_trace()
    prob.solve(v=False)
    Res = Results(prob, N)
    name_key = f'nocap'
    output[name_key] = P2P_OPFloss_save(Res, Tsocial=0, Dsocial=0, cap=False)

    # prob.allocation_constraints(N, s_t=1, s_d=1, ref=True)
    # prob.solve(v=False)
    # Res = Results(prob, N)
    # name_key = f'ref'
    # output[name_key] = P2P_OPFloss_save(Res, Tsocial=1, Dsocial=1, ref=True)

    # for Ts in TSO_socials:
    #     for Ds in DSO_socials:
    #         name_key = f'Ts{Ts:.2f}_Ds{Ds:.2f}'
    #         prob.allocation_constraints(N, s_t=Ts, s_d=Ds)
    #         prob.solve(v=False)
    #         Res = Results(prob, N)
    #         output[name_key] = P2P_OPFloss_save(Res, Tsocial=Ts, Dsocial=Ds)
    #         print(name_key)
